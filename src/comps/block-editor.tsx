import {
	BlockEditorProvider,
	BlockList,
	// @ts-expect-error: ignore
	BlockTools,
	WritingFlow,
	ObserveTyping,
} from '@wordpress/block-editor'
import type { BlockInstance } from '@wordpress/blocks'
import { SlotFillProvider, Popover } from '@wordpress/components'
import { useState } from 'react'
import '@wordpress/components/build-style/style.css'
import '@wordpress/block-editor/build-style/style.css'

import { registerCoreBlocks } from '@wordpress/block-library'

// Make sure to load the block stylesheets too
import '@wordpress/block-library/build-style/style.css'
import '@wordpress/block-library/build-style/editor.css'
import '@wordpress/block-library/build-style/theme.css'

import { registerBlockType } from '@wordpress/blocks'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const self2: any = window

if (!self2['~/block-editor-20210817/src/comps/block-editor.tsx']) {
	self2['~/block-editor-20210817/src/comps/block-editor.tsx'] = 1
	registerCoreBlocks()
	registerBlockType('ytoune/test-hoge', {
		attributes: {},
		category: 'text',
		title: 'Test Hoge',
		save: () => (
			<div style={{ color: 'red' }} title="自作のブロックタイプ">
				hoge 自作のブロックタイプテスト
			</div>
		),
	})
}

export const BlockEditor = () => {
	const [blocks, updateBlocks] = useState<BlockInstance[]>(initBlocks)
	console.log(blocks)
	return (
		<div>
			<BlockEditorProvider
				value={blocks}
				onInput={blocks => updateBlocks(blocks)}
				onChange={blocks => updateBlocks(blocks)}
			>
				<SlotFillProvider>
					<BlockTools>
						<WritingFlow>
							<ObserveTyping>
								<BlockList />
							</ObserveTyping>
						</WritingFlow>
					</BlockTools>
					<Popover.Slot />
				</SlotFillProvider>
			</BlockEditorProvider>
		</div>
	)
}

export default BlockEditor

const initBlocks = [
	{
		clientId: '9d9b59ec-6f65-4a01-a9a1-01e2b71b3bb4',
		name: 'core/heading',
		isValid: true,
		attributes: {
			content: 'ブロックエディタ使ってみるデモ',
			level: 2,
		},
		innerBlocks: [],
	},
	{
		clientId: 'cdc4c7c1-2f55-4122-a978-14e9dfd72dac',
		name: 'core/list',
		isValid: true,
		attributes: {
			ordered: false,
			values: '<li>hoge</li><li>fuga</li><li>piyo</li>',
		},
		innerBlocks: [],
	},
	{
		clientId: 'bbde79b8-b769-47c9-85cb-74fec6ffabc1',
		name: 'core/paragraph',
		isValid: true,
		attributes: {
			content: 'あかさたな',
			dropCap: false,
		},
		innerBlocks: [],
	},
	{
		clientId: '2aa82054-550f-4a1f-9c05-69359559736b',
		name: 'core/columns',
		isValid: true,
		attributes: {
			isStackedOnMobile: true,
		},
		innerBlocks: [
			{
				clientId: '31b101e1-d989-42d9-b75e-2ec859f59f08',
				name: 'core/column',
				isValid: true,
				attributes: {},
				innerBlocks: [
					{
						clientId: 'c6051b37-a5c1-4c7b-9528-2fd6a241d392',
						name: 'core/paragraph',
						isValid: true,
						attributes: {
							content: '右に表',
							dropCap: false,
						},
						innerBlocks: [],
					},
				],
			},
			{
				clientId: 'a88ea596-1a60-4c2e-9270-6c19a4a1d7d5',
				name: 'core/column',
				isValid: true,
				attributes: {},
				innerBlocks: [
					{
						clientId: '4575863c-34bd-45ca-a452-0df613be8a28',
						name: 'core/table',
						isValid: true,
						attributes: {
							hasFixedLayout: false,
							caption: 'メンバー',
							head: [],
							body: [
								{
									cells: [
										{
											content: '名前',
											tag: 'td',
										},
										{
											content: '性別',
											tag: 'td',
										},
										{
											content: '年齢',
											tag: 'td',
										},
									],
								},
								{
									cells: [
										{
											content: '田中',
											tag: 'td',
										},
										{
											content: '男',
											tag: 'td',
										},
										{
											content: '32',
											tag: 'td',
										},
									],
								},
								{
									cells: [
										{
											content: '鈴木',
											tag: 'td',
										},
										{
											content: '女',
											tag: 'td',
										},
										{
											content: '35',
											tag: 'td',
										},
									],
								},
								{
									cells: [
										{
											content: '木村',
											tag: 'td',
										},
										{
											content: '男',
											tag: 'td',
										},
										{
											content: '39',
											tag: 'td',
										},
									],
								},
							],
							foot: [],
						},
						innerBlocks: [],
					},
				],
			},
		],
	},
	{
		clientId: '1f0fc390-e630-4465-a663-4966c607e646',
		name: 'core/paragraph',
		isValid: true,
		attributes: {
			content: 'ああああああああああああ',
			dropCap: false,
		},
		innerBlocks: [],
	},
	{
		clientId: '22f5afed-46ed-4da9-b267-217c2f619a6f',
		name: 'core/paragraph',
		isValid: true,
		attributes: {
			content: 'いいいいいいいいいいい',
			dropCap: false,
		},
		innerBlocks: [],
	},
	{
		clientId: '013579cd-a038-4d33-b3fd-28a12224abc1',
		name: 'ytoune/test-hoge',
		isValid: true,
		attributes: {},
		innerBlocks: [],
	},
]
