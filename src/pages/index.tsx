import dynamic from 'next/dynamic'

const DynamicComponentWithNoSSR = dynamic(
	() => import('../comps/block-editor'),
	{ ssr: false },
)

const Home = () => {
	return (
		<div>
			<DynamicComponentWithNoSSR />
		</div>
	)
}

export default Home
