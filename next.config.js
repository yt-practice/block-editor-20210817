new Function('return this')().Element = class ElementMock {}
const basePath =
	'development' === process.env.NODE_ENV ? '' : '/block-editor-20210817/'

module.exports = {
	basePath: basePath.replace(/\/$/, ''),
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	trailingSlash: true,
}
